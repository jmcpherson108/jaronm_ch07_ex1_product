import java.text.NumberFormat;

public class Product {

    private String code;
    private String description;
    private double price;

    public Product() {
        code = "";
        description = "";
        price = 0;
    }

    // ex 7-1, step 5: write a 3-arg overloaded constructor
   public Product(String code, String description, double price) {
       this.code = code;
       this.description = description;
       this.price = price;
   } 
    
    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public String getPriceFormatted() {
        NumberFormat currency = NumberFormat.getCurrencyInstance();
        return currency.format(price);
    }
    
    // ex 7-1 step 8 -- use number formatting, not currency formatting
    public String getPriceNumberFormat() {
     NumberFormat number = NumberFormat.getNumberInstance();
     number.setMinimumFractionDigits(2);
     number.setMaximumFractionDigits(2);
     String priceFormatted = number.format(price);
     return priceFormatted;
    } // end method getPriceNumber format
}