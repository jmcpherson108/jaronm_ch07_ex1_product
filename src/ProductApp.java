import java.util.Scanner;

public class ProductApp {

    public static void main(String args[]) {
        // display a welcome message
        System.out.println("Welcome to the Product Selector");
        System.out.println();

        // display 1 or more products
        Scanner sc = new Scanner(System.in);
        String choice = "y";
        while (choice.equalsIgnoreCase("y")) {
            // get the input from the user
            System.out.print("Enter product code: ");
            String productCode = sc.next();  // read the product code
            sc.nextLine();  // discard any other data entered on the line

            // get the Product object
            // ex 7-1 step 12: work with an INSTANCE
            // of the ProductDB class (think of this object
            // as a CONNECTION to the database)
            ProductDB db = new ProductDB();
            Product product = db.getProduct(productCode);

            // display the output
            System.out.println();
            System.out.println("SELECTED PRODUCT");
            System.out.println("Description: " + product.getDescription());
            // ex 7-1 step 9: use getPriceNumberFormat
            //System.out.println("Price:       " + product.getPriceFormatted());
            System.out.println("Price:       " + product.getPriceNumberFormat());
            System.out.println();

            // see if the user wants to continue
            System.out.print("Continue? (y/n): ");
            choice = sc.nextLine();
            System.out.println();
        }
    }
}