public class ProductDB {

    // ex 7-1 step 11: make this an instance (non-static) method
    public Product getProduct(String productCode) {
        // In a more realistic application, this code would
        // get the data for the product from a file or database
        // For now, this code just uses if/else statements
        // to return the correct product data

        // create the Product object
        // ex 7-1, step 6: use the newly defined
        // 3-arg constructor to initialize the object
        //Product product = new Product();
        Product product;
        
        // fill the Product object with data
        //product.setCode(productCode);
        if (productCode.equalsIgnoreCase("java")) {
            product = new Product("java", "Murach's Java Programming", 57.50);
            //product.setDescription("Murach's Java Programming");
            //product.setPrice(57.50);
        } else if (productCode.equalsIgnoreCase("jsp")) {
            product = new Product("jsp", "Murach's Java Servlets and JSP", 57.50);
            //product.setDescription("Murach's Java Servlets and JSP");
            //product.setPrice(57.50);
        } else if (productCode.equalsIgnoreCase("mysql")) {
            product = new Product("mysql", "Murach's MySQL", 54.50);
            //product.setDescription("Murach's MySQL");
            //product.setPrice(54.50);
        } else if (productCode.contains("andr")) {
            product = new Product("andr", "Murach's Android Programming",57.50);
            //product.setDescription("Murach's Android Programming");
            //product.setPrice(57.50);
        }
        else {
            //product.setDescription("Unknown");
            product = new Product(productCode,"Unknown",0.0);
        }
        return product;
    }
}